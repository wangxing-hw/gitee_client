use gitee_client::GiteeClient;
use structopt::StructOpt;
use tokio;

#[derive(StructOpt, Debug)]
#[structopt(
    name = "Gitee Client Example",
    about = "A command-line interface for interacting with Gitee"
)]
struct Opt {
    #[structopt(short, long, help = "Sets the Gitee access token")]
    access_token: String,

    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt, Debug)]
enum Command {
    // ...
    #[structopt(name = "enterprise_prs", about = "Get enterprise pull requests")]
    EnterprisePRs {
        #[structopt(long, help = "Sets the enterprise name for which to retrieve PRs")]
        enterprise: String,

        #[structopt(long, help = "Issue number filter")]
        issue_number: Option<u64>,

        #[structopt(long, help = "Repository filter")]
        repo: Option<String>,

        #[structopt(long, help = "Program ID filter")]
        program_id: Option<u64>,

        #[structopt(long, help = "State filter")]
        state: Option<String>,

        #[structopt(long, help = "Head branch filter")]
        head: Option<String>,

        #[structopt(long, help = "Base branch filter")]
        base: Option<String>,

        #[structopt(long, help = "Sort order")]
        sort: Option<String>,

        #[structopt(long, help = "Filter PRs since a timestamp")]
        since: Option<String>,

        #[structopt(long, help = "Direction filter")]
        direction: Option<String>,

        #[structopt(long, help = "Milestone number filter")]
        milestone_number: Option<u64>,

        #[structopt(long, help = "Labels filter")]
        labels: Option<String>,

        #[structopt(long, help = "Page number")]
        page: Option<u32>,

        #[structopt(long, help = "Number of PRs per page")]
        per_page: Option<u32>,
    },
}

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();
    let client = GiteeClient::new(&opt.access_token);

    match opt.cmd {
        // ...
        Command::EnterprisePRs {
            enterprise,
            issue_number,
            repo,
            program_id,
            state,
            head,
            base,
            sort,
            since,
            direction,
            milestone_number,
            labels,
            page,
            per_page,
        } => {
            let enterprise_prs_result = client
                .get_enterprise_prs(
                    &enterprise,
                    issue_number,
                    repo.as_deref(),
                    program_id,
                    state.as_deref(),
                    head.as_deref(),
                    base.as_deref(),
                    sort.as_deref(),
                    since.as_deref(),
                    direction.as_deref(),
                    milestone_number,
                    labels.as_deref(),
                    page,
                    per_page,
                )
                .await;

            match enterprise_prs_result {
                Ok(enterprise_prs) => {
                    println!("Enterprise PRs: {:?}", enterprise_prs);
                }
                Err(error) => {
                    eprintln!("Error getting enterprise PRs: {}", error);
                }
            }
        }
    }
}
