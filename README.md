# Gitee Client

Gitee Client 是一个使用 Rust 编写的命令行工具和库，用于与 Gitee API 交互。它支持查询用户、企业和仓库下的所有 Pull Requests，以及获取满足条件的 PR 的详细信息。

## 功能

- 查询用户、企业和仓库下的所有 Pull Requests
- 查询每个 Pull Request 的详细信息
- 获取满足条件的 PR 的详细信息列表

## 使用

首先，确保您已安装 [Rust](https://www.rust-lang.org/tools/install)。然后，使用以下命令克隆仓库并构建命令行工具：

```bash
git clone https://gitee.com/wangxing-hw/gitee_client.git
cd gitee_client
cargo build --release
```

构建完成后，您可以在 `target/release` 目录下找到名为 `gitee_client_cli` 的可执行文件。将其添加到您的 `PATH` 中，或者直接执行它。

### 命令行参数

```text
USAGE:
    gitee_client_cli [OPTIONS] <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -t, --access-token <ACCESS_TOKEN>    Sets the Gitee access token

SUBCOMMANDS:
    enterprise_prs    Get enterprise pull requests
    help              Prints this message or the help of the given subcommand(s)
```

### 示例

以下示例演示了如何使用 `gitee_client_cli` 查询企业 Pull Requests。

首先，设置您的 Gitee Access Token：

```bash
export GITEE_ACCESS_TOKEN=your_access_token
```

查询企业 Pull Requests：

```bash
gitee_client_cli -t $GITEE_ACCESS_TOKEN enterprise_prs --enterprise your_enterprise_name
```

查询特定仓库的 Pull Requests：

```bash
gitee_client_cli -t $GITEE_ACCESS_TOKEN enterprise_prs --enterprise your_enterprise_name --repo your_repo_name
```

查询带有特定标签的 Pull Requests：

```bash
gitee_client_cli -t $GITEE_ACCESS_TOKEN enterprise_prs --enterprise your_enterprise_name --labels bug,feature
```

有关更多选项和参数，请参阅命令行帮助：

```bash
gitee_client_cli --help
```