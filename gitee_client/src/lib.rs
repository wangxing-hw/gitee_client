use reqwest::{Client, Error};
use serde::{Deserialize, Serialize};
use std::time::Duration;


const GITEE_API_BASE: &str = "https://gitee.com/api/v5";



pub struct GiteeClient {
    client: Client,
    access_token: String,
}

fn add_url_param(url: &mut String, name: &str, value: Option<impl ToString>) {
    if let Some(value) = value {
        url.push_str(&format!("&{}={}", name, value.to_string()));
    }
}

impl GiteeClient {
    pub fn new(access_token: &str) -> Self {
        let client = reqwest::Client::builder()
        .timeout(Duration::from_secs(10)) // 设置超时为 10 秒
        .build()
        .unwrap();

        let access_token = access_token.to_string();
        GiteeClient {
            client,
            access_token,
        }
    }

    pub async fn get_enterprise_prs(
        &self,
        enterprise: &str,
        issue_number: Option<u64>,
        repo: Option<&str>,
        program_id: Option<u64>,
        state: Option<&str>,
        head: Option<&str>,
        base: Option<&str>,
        sort: Option<&str>,
        since: Option<&str>,
        direction: Option<&str>,
        milestone_number: Option<u64>,
        labels: Option<&str>,
        page: Option<u32>,
        per_page: Option<u32>,
    ) -> Result<Vec<PullRequest>, Box<dyn std::error::Error>> {
        let mut url = format!(
            "{}/enterprise/{}/pull_requests?access_token={}",
            GITEE_API_BASE, enterprise, self.access_token
        );

        add_url_param(&mut url, "issue_number", issue_number);
        add_url_param(&mut url, "repo", repo);
        add_url_param(&mut url, "program_id", program_id);
        add_url_param(&mut url, "state", state);
        add_url_param(&mut url, "head", head);
        add_url_param(&mut url, "base", base);
        add_url_param(&mut url, "sort", sort);
        add_url_param(&mut url, "since", since);
        add_url_param(&mut url, "direction", direction);
        add_url_param(&mut url, "milestone_number", milestone_number);
        add_url_param(&mut url, "labels", labels);
        add_url_param(&mut url, "page", page);
        add_url_param(&mut url, "per_page", per_page);
        let response = self.client.get(&url).send().await?;
        let status = response.status();

        if response.status().is_success() {
            println!("code is {}", status);
            let response_text = response.text().await?;
            //println!("Server response: {}", response_text);
            let prs: Vec<PullRequest> = serde_json::from_str(&response_text)?;
            Ok(prs)
        } else {
            let error_text = response.text().await?;
            Err(format!(
                "API request failed with status {}: {}",
                status,
                error_text
            )
            .into())
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PullRequest {
    html_url: String,
    url: String,
    created_at: String,
    title: String,
    state: String,
    merged_at: Option<String>,
}
